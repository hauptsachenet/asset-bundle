/**
 * User: Marco Pfeiffer
 * Date: 09.04.13
 * Time: 09:43
 */

// creates a fileuploader area
// calls events:
//  upload-start : as soon as the first upload begins
//  upload-end : if the last upload has ended
//  file-drop : on every new file that was dropped which is valid. If preventDefault is called the upload won't be started
//              this event gets a parameter with the file that will be uploaded. Note that this doesn't work with iframes.
//  file-thumbed : as soon as a thumbnail is available. This can happen before, during and after the upload.
//  file-remove : if a file gets removed from the dropzone
//  dragenter : if a file is held over the field or the input field get's clicked (file dialog open)
//  dragleave : as soon as the file is outside the area. This only works with html5 drop support
(function ( $ ) {

    var requiredProps = "[data-upload-url][data-field-name]";
    var validateProps = [
        "accept-type",
        "maximum-size",
        "min-width",
        "max-width",
        "min-height",
        "max-height"
    ];
    var isImage = /^image\/[\w-]+/im;
    var sfFormName = "asset";
    var sfUploadName = sfFormName + "[files][]";
    var uid = 0;

    /**
     * Utility function too format a size in bytes.
     * @param {int|float} size
     * @returns {string}
     */
    function formatByteSize ( size ) {
        var units = ["B", "kB", "MB", "GB", "TB"];
        var useUnit = 0;
        while ( size >= 1000 && useUnit < units.length ) {
            useUnit++;
            size /= 1024;
        }
        return Math.ceil( size ).toFixed( 0 ) + " " + units[ useUnit ];
    }

    /**
     * Returns an url to an icon representing the given mime type.
     * Icons by: http://openiconlibrary.sourceforge.net/
     * @param {string} type the mime type to represent
     * @returns {string}
     */
    function mimeIcon ( type ) {
        var baseUrl = "/bundles/hnasset/img/fileIcons/";
        var types = type.match( /^([^\/]+)\/(.+)$/m ) || [];
        switch ( types[1] ) {
            case "application":
                switch ( types[2] ) {
                    case "pdf":
                        return baseUrl + "pdf.png";
                }
                break;
            case "video":
                return baseUrl + "video.png";
            case "image":
                return baseUrl + "image.png";
        }
        // if nothing worked
        return baseUrl + "file.png";
    }

    /**
     * Uses an Iframe to upload content from an input field.
     * Note that the fileInput will be consumend and removed afterwards.
     * @param {Element} fileInput
     * @param {Object} params Additional params to send with
     * @param {string} uploadUrl
     * @param {function(object)} onload A callback with json
     * @param {function()} onerror A callback if the request failed
     */
    function iframeUpload ( fileInput, params, uploadUrl, onload, onerror ) {
        var id = uid++;
        var iframeId = "uploadIframe" + id;

        // create a form for this submit
        var form = $( '<form method="post" enctype="multipart/form-data"></form>' )
            .prop( { target: iframeId, action: uploadUrl } ).appendTo( "body" );

        // create an iframe as a target for this upload
        var iFrame = $( '<iframe style="width: 1; height: 1; visibility: hidden"></iframe>' )
            .prop( { id: iframeId, name: iframeId } ).appendTo( "body" )
            .on( "load", function () {
                var data = iFrame.contents().text();
                try {
                    data = $.parseJSON( data );
                } catch ( e ) {
                    onerror();
                }
                if ( typeof data != "string" ) {
                    onload( data );
                }
            } )
            .on( "error", function () {
                onerror();
            } )
            .on( "error load", function () {
                iFrame.add( form ).remove();
            } );

        // prepare the fileInput
        fileInput.prop( { name: sfUploadName } ).appendTo( form );

        // create additional params to go with it
        for ( var key in params ) {
            $( '<input type="hidden" />' ).prop( {
                name: sfFormName + "[" + key + "]",
                value: params[key]
            } ).appendTo( form );
        }

        // submit the form after a minimum delay
        form.submit();
    }

    /**
     * Uploads a file using ajax
     * @param {File} file The file that has to be uploaded
     * @param {Object} params Additional params to send with
     * @param {string} uploadUrl The url the upload should go to
     * @param {function(Object)} onload Will be called if done
     * @param {function(int)} onerror Will be called if a bad server status was returned
     * @param {function(float)} onprogress Will be called on every tcp package that was send
     * @returns {XMLHttpRequest} The xhr that was used for this request.
     */
    function ajaxUpload ( file, params, uploadUrl, onload, onerror, onprogress ) {
        var form = new FormData;
        var xhr = new XMLHttpRequest;
        form.append( sfUploadName, file );
        for ( var key in params ) {
            form.append( sfFormName + "[" + key + "]", params[key] );
        }

        xhr.onreadystatechange = function () {
            if ( xhr.readyState != 4 ) return;
            if ( xhr.status != 200 ) onerror.call( this, xhr.status );
            else try {
                onload.call( this, $.parseJSON( xhr.responseText ) );
            } catch ( e ) {
                console.error( e );
                onerror.call( this, 500 );
            }
        };
        xhr.upload.onprogress = function ( e ) {
            onprogress.call( xhr, Math.min( 1.0, e.total / file.size ) * 100.0 );
        }
        xhr.open( "POST", uploadUrl, true );
        xhr.send( form );
        return xhr;
    }

    ///////////////////
    // SUPPORT CHECK //
    ///////////////////

    // check if we can access the files of an file input
    // if not the iframe way has to be used
    $.support.fileAccess = "File" in window;
    $.support.fileReader = "FileReader" in window; // if a file can be read
    $.support.formData = "FormData" in window; // if a form can be send via XMLHttpRequest
    $.support.eventListener = "addEventListener" in document;

    /////////////////////
    // BEHAVIOR CHANGE //
    /////////////////////

    // prevent normal filedrop resulting into relocation
    if ( $.support.eventListener ) {
        // prevent normal drop behavior (native way as jquery fails to preventDefault on drop events)
        $.each( "dragenter dragover dragleave drop".split( " " ), function ( i, evtName ) {
            document.addEventListener( evtName, function ( e ) {
                e.preventDefault();
                e.stopPropagation();
            }, false );
        } );
    }
    // also prevent form submit if .filedrop.uploading is in it
    $( document ).on( "submit", "form:has(.filedrop.uploading)", function () {
        var form = $( this );
        var submitButtons = form.find( "input[type=submit]" );

        // submit ass soon as done
        form.on( "upload-end.filedrop", function () {
            if ( form.find( ".filedrop.uploading" ).length == 0 ) {
                form.trigger( "submit" );
            }
        } );

        submitButtons.each( function () {
            var btn = $( this );
            var beforeText = btn.val();
            btn.val( "waiting for upload..." );
            btn.on( "revert-autosubmit.filedrop", function () {
                form.off( "upload-end.filedrop" );
                btn.val( beforeText );
            } );
        } );

        submitButtons.on( "click.filedrop", function ( e ) {
            e.preventDefault();
            submitButtons.trigger( "revert-autosubmit" ).off( ".filedrop" );
        } );
    } );


    ///////////////////////////////
    // FILEDROP JQUERY EXTENSION //
    ///////////////////////////////

    /**
     * The jquery extension.
     * It has no parameters and no way of removing it... yet
     */
    $.fn.filedrop = function () {
        this.filter( requiredProps ).each( function () {

            var dropZone = $( this );
            var name = dropZone.data( "field-name" );
            var uploadUrl = dropZone.data( "upload-url" );
            var multiple = !!dropZone.data( "multiple-file" ) || false;
            var acceptType = new RegExp( "^" + (dropZone.data( "accept-type" ) || ".*/.*") + "$" );
            var maximumSize = dropZone.data( "maximum-size" ) || 50 * 1024 * 1024; // 10 MB

            var params = {};

            // fill params for the request
            for ( var i = 0; i < validateProps.length; ++i ) {
                var data = dropZone.data( validateProps[i] );
                if ( data != null ) params[validateProps[i]] = data.toString();
            }

            var fileInput; // used to make dragging simple and allow click
            var uploadsRunning = 0; // simple counter to prevent submit while uploading
            function changeUploadCounter ( number ) {
                if ( number > 0 && uploadsRunning <= 0 ) {
                    dropZone.addClass( "uploading" ).trigger( "upload-start" );
                }
                uploadsRunning += number;
                if ( number < 0 && uploadsRunning <= 0 ) {
                    dropZone.removeClass( "uploading" ).trigger( "upload-end" );
                }
            }

            /**
             * Handles an array of File instances
             * @param files FileList
             */
            function createByFiles ( files ) {
                // if no multiple is allowed but there are already files, kill 'em
                if ( !multiple ) {
                    dropZone.find( ".dropzone-item" ).remove();
                    files.length = Math.min( 1, files.length );
                }

                var fileLength = Math.min( files.length, multiple ? 99 : 1 );
                for ( var i = 0; i < files.length; i++ ) {
                    var file = files[i];

                    // check via event if the file should be accepted
                    // if it should be done create file item
                    $( document ).on( "file-drop.defaultBehavior", function ( e ) {
                        if ( e.isDefaultPrevented() ) return;

                        // create item
                        var fileItem = new FileItem;
                        fileItem.setFile( file );
                        dropZone.append( fileItem.getElement() ).trigger( "new-file" );

                        // check the type
                        if ( !acceptType.test( file.type ) ) {
                            console.error( file.type + " - " + acceptType );
                            fileItem.failed( "wrong type" );
                        }
                        // check the size
                        else if ( maximumSize < file.size ) {
                            console.error( file.size + " - " + maximumSize );
                            fileItem.failed( "max " + formatByteSize( maximumSize ) );
                        }
                        // else start the upload
                        else {
                            changeUploadCounter( +1 );
                            fileItem.upload( uploadUrl, params, name, function ( success ) {
                                changeUploadCounter( -1 );
                            } );
                        }
                    });

                    // trigger events to check if this should be done
                    dropZone.trigger( "file-drop", file );
                    $( document ).off( "file-drop.defaultBehavior" );
                }
            }

            // interpret existing items
            $( ".dropzone-item" ).each( function () {
                new FileItem( this );
            } );

            // prepare the field
            fileInput = $( '<input type="file" class="fullsize-file-input" />' )
                .prop( "multiple", multiple ).appendTo( dropZone );

            // if event listener is supported we can try to use drop events
            if ( $.support.eventListener ) {
                // set event so we can use css on file drop
                dropZone.on( "dragenter dragleave drop", function ( e ) {
                    dropZone[ (e.type === "dragenter" ? "add" : "remove") + "Class" ]( "drop-over" );
                } );
                dropZone.on( "drop", function ( e ) {
                    var evt = e.originalEvent;
                    createByFiles( "dataTransfer" in evt ? evt.dataTransfer.files : evt.target.files );
                } );
            }

            // trigger dragenter event if the input was clicked
            fileInput.on( "click.dragenter.filedrop", function ( evt ) {
                fileInput.trigger( "dragenter" );
            } );

            // if the file input changes handle the files
            fileInput.on( "change.filedrop", function () {
                // if file access is supported create FileItem instances for upload handle
                if ( $.support.fileAccess && $.support.formData ) {
                    createByFiles( fileInput[0].files );
                    fileInput.val( "" );
                }
                // else start an upload over an iframe
                else {
                    // create a clone of the input so we can use the current one in another form
                    var toSend = fileInput.detach();
                    fileInput = toSend.clone( true ).val( "" ).appendTo( dropZone );

                    // TODO indicator that upload is running
                    changeUploadCounter( +1 );
                    iframeUpload( toSend, params, uploadUrl, function ( response ) {
                        changeUploadCounter( -1 );
                        // if the error array isn't empty this failed, else success
                        if ( !response.errors || response.errors.length || response.files.length == 0 ) {
                            alert( (response.errors || []).join( "\n" ) );
                        } else {
                            // create thumbs for each file
                            for ( var i = 0; i < response.files.length; i++ ) {
                                var file = response.files[i];
                                var fileItem = new FileItem;
                                fileItem.uploadDone( file.id, name );
                                fileItem.setThumb( file.thumb, file.path, true );
                                dropZone.append( fileItem.getElement() );
                            }
                        }
                    }, function () {
                        changeUploadCounter( -1 );
                        alert( "server error" );
                    } );
                }
            } );

            // parse already existing images
            dropZone.find( "dropzone-item" ).each( function () {
                var fileItem = new FileItem( this );
                dropZone.append( fileItem.getElement() ).trigger( "new-file" );
            } );

        } );
    };

    /////////////////////
    // FILE ITEM CLASS //
    /////////////////////

    /**
     * A single item in the drop zone.
     * <div class="dropzone-item">
     *     <a class="close"></a>
     *     <a class="item-image" target="_blank" href="{resource href}">
     *         <img src="" />
     *     </a>
     *     <div class="item-info">
     *         <div class="item-text">0-99%/done!/failed!/{error messages}</div>
     *         <div class="item-progress">
     *             <div class="item-progress-bar" style="width: 0-100%"></div>
     *         </div>
     *     </div>
     *     <input type="hidden" name="{formName}" value="{idFromServer}" />
     * </div>
     * @param element Element|null If this is an already uploaded file, the element it is in.
     *                      This element will be removed if the image isn't wanted anymore.
     * @constructor
     */
    function FileItem ( element ) {
        var self = this;
        this.file = null; // can contain a File instance
        this.thumbed = false; // if true the final thumbnail was added

        if ( element != null ) {
            this.itemElement_ = $( element );
        } else {
            // create a bunch of items needed for a nice uploader
            this.itemElement_ = $( '<div class="dropzone-item"></div>' );
            $( '<a class="close"></a>' ).appendTo( this.itemElement_ );

            this.imagePlace_ = $( '<a class="item-image" target="_blank"></a>' ).appendTo( this.itemElement_ );

            this.itemInfoElement_ = $( '<div class="item-info"></div>' ).appendTo( this.itemElement_ );
            this.itemTextField_ = $( '<div class="item-text">starting...</div>' ).appendTo( this.itemInfoElement_ );
            var progressWraper = $( '<div class="item-progress"></div>' ).appendTo( this.itemInfoElement_ );
            this.itemProgressElement_ = $( '<div class="item-progress-bar"></div>' ).appendTo( progressWraper );
        }

        // close button
        this.itemElement_.on( "click", ".close", function () {
            $( document ).on( "file-remove.defaultBehavior", function ( e ) {
                if ( e.isDefaultPrevented() ) return;
                self.itemElement_.remove();
                // if an xhr is running stop it
                if ( this.xhr != null ) {
                    this.xhr.abort();
                }
            } );
            self.itemElement_.trigger( "file-remove" );
        } );
    }

    FileItem.prototype = {

        /**
         * Returns the element which is this item.
         * This item will receive all updates etc.
         * @returns Object
         */
        getElement: function () {
            return this.itemElement_;
        },

        /**
         * Sets a file for this item.
         * @param {File} file
         * @throws {TypeError} if the file is not an instance of File
         */
        setFile: function ( file ) {
            if ( !(file instanceof File) ) throw new TypeError( "file is not an instanceof File" );
            this.file = file;
            this.setFileAsThumb( file );
        },

        /**
         * Uses a file reader to set a thumb as image
         * @param {File} file
         */
        setFileAsThumb: function ( file ) {
            var self = this;
            if ( $.support.fileReader && !this.thumbed && isImage.test( file.type ) ) {
                var reader = new FileReader;
                reader.onload = function () {
                    self.setThumb( this.result, null, true );
                };
                reader.readAsDataURL( file );
            } else {
                this.setThumb( mimeIcon( file.type ) );
            }
        },

        /**
         * Sets the current thumb and the url of it
         * @param {string} src The url to the thumb
         * @param {string} [href] The link on it
         * @param {boolean} [keep] If true the image won't change, however the href can
         */
        setThumb: function ( src, href, keep ) {
            this.imagePlace_.prop( { href: href || "#" } );
            // only add the image if there is none yet
            if ( src && !this.thumbed ) {
                var image = new Image;
                image.src = src;
                this.imagePlace_.empty().append( image );
                this.imagePlace_.trigger( "file-thumbed" );
                this.thumbed = !!keep;
            }
        },

        /**
         * Uploads the file with an XMLHttpRequest or a new iframe.
         * @param {string} url The url where the upload should go to
         * @param {Object} params additional params to got with the request
         * @param {string} name The name of the hidden input for the image id.
         * @param {function(bool)} callback A callback that will be called after the upload is done or failed
         * @throws {Error} if no file was set
         */
        upload: function ( url, params, name, callback ) {
            if ( this.file == null ) throw new Error( "Upload not possible, file was not set" );
            var self = this;

            // start request
            self.xhr = ajaxUpload( self.file, params, url, function ( response ) {
                // if the error array isn't empty this failed, else success
                if ( !response.errors || response.errors.length || response.files.length == 0 ) {
                    alert( response.errors.join( "\n" ) );
                    self.failed( "failed!" );
                    callback.call( self.xhr, false );
                } else if ( response.warnings.length ) {
                    // if there are warnings according to ticket
                    // #3207 validierung bild upload allgemein
                    // the user still can choose to upload the file
                    if (confirm( response.warnings.join( "\n" ) )) {
                        self.uploadDone( response.files[0].id, name );
                        self.setThumb( response.files[0].thumb, response.files[0].path, true );
                        callback.call( self.xhr, true );
                    } else {
                        self.failed( "failed!" );
                        callback.call( self.xhr, false );
                    }
                } else {
                    self.uploadDone( response.files[0].id, name );
                    self.setThumb( response.files[0].thumb, response.files[0].path, true );
                    callback.call( self.xhr, true );
                }
            }, function ( status ) {
                self.fatalError( status );
                callback.call( self.xhr, false );
            }, function ( progress ) {
                self.progress( progress );
                callback.call( self.xhr, false );
            } );
        },

        /**
         * Sets the progress to an amount in percent.
         * @param {float} progress Progress in percent
         */
        progress: function ( progress ) {
            this.itemProgressElement_.css( { width: progress + "%" } );
            this.itemTextField_.text( progress.toFixed( 0 ) + "%" );
        },

        /**
         * Will be called once the request is done.
         * @param {string|int} fileId The id that will be in the input field
         * @param {string} name The name of the input field
         */
        uploadDone: function ( fileId, name ) {
            this.itemProgressElement_.css( { width: "100%" } );
            this.itemTextField_.text( "done!" );
            this.itemInfoElement_.delay( 1000 ).fadeOut();

            // create hidden input for file
            $( '<input type="hidden" />' )
                .prop( { name: name, value: fileId } )
                .appendTo( this.itemElement_ );
        },

        /**
         * Will be called if the upload failed with a reason
         * @param {string} error
         */
        failed: function ( error ) {
            this.itemProgressElement_.css( { width: "100%" } ).addClass( "failed" );
            this.itemTextField_.text( error );
        },

        /**
         * Will be called if the upload failed with a bad Server status
         * @param {string|int} status
         */
        fatalError: function ( status ) {
            this.itemProgressElement_.css( { width: "100%" } ).addClass( "failed" );
            this.itemTextField_.text( "failed! (" + status + ")" );
        }
    };


    // autospawn
    $( document ).on( "ready update.filedrop", function ( e ) {
        $( "div" + requiredProps, e.target ).filedrop();
    } );
})( jQuery );