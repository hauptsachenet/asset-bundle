<?php

namespace Hn\AssetBundle\Form;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Hn\AssetBundle\Form\Transformer\AssetIdTransformer;
use Hn\AssetBundle\Form\Transformer\AssetMultipleIdTransformer;
use Symfony\Bridge\Doctrine\Form\EventListener\MergeDoctrineCollectionListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AssetType extends AbstractType
{

    // some constants to feed the type parameter with
    const EVERYTHING = ".*/.*";
    const IMAGE = "image/(png|jpe?g|pjpeg|gif|(x-)?tiff|bmp|x-icon)";
    const PDF = "application/pdf";
    const VIDEO = "video/.*"; // TODO more percise
    const VISUAL = "image/(png|jpe?g|pjpeg|gif|(x-)?tiff|bmp|x-icon)|application/pdf";

    public static function typeToRegExp($type = AssetType::IMAGE)
    {
        return "/^" . addcslashes($type, "/") . "$/im";
    }

    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getName()
    {
        return 'hn_assetbundle_asset';
    }

    public function getParent()
    {
        return "hidden";
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['multiple']) {
            $transformer = new AssetMultipleIdTransformer($this->em);
            $builder->addViewTransformer($transformer);
            $builder->addEventSubscriber(new MergeDoctrineCollectionListener());
        } else {
            $transformer = new AssetIdTransformer($this->em);
            $builder->addViewTransformer($transformer);
        }
    }


    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars["multiple"] = $options["multiple"];
        $data = $form->getData();
        $view->vars["assets"] = ($data instanceof \Traversable || is_array($data))
            ? $data : ($data !== null ? array($data) : array());

        $view->vars["attr"]["class"] = "filedrop";
        $view->vars["placeholder"] = $options["placeholder"] ? $options["placeholder"]
            : ($options["multiple"] ? "drop files here" : "drop file here");

        $view->vars["fileUploadAction"] = "asset_upload";
        $view->vars["fileUploadParams"] = array();

        $view->vars["attr"]["data-accept-type"] = $options["acceptType"];
        $view->vars["attr"]["data-maximum-size"] = $options["maximumSize"];

        $view->vars["attr"]["data-min-width"] = $options["minWidth"];
        $view->vars["attr"]["data-max-width"] = $options["maxWidth"];
        $view->vars["attr"]["data-min-height"] = $options["minHeight"];
        $view->vars["attr"]["data-max-height"] = $options["maxHeight"];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'class' => 'HnAssetBundle:Asset',
                'required' => false,
                'multiple' => false,
                'property' => 'id',
                'placeholder' => '',

                'acceptType' => self::IMAGE,
                'maximumSize' => 50000000, // 50 MB

                'minWidth' => null,
                'maxWidth' => null,
                'minHeight' => null,
                'maxHeight' => null
            )
        );
    }
}
