<?php
/**
 * Created by JetBrains PhpStorm.
 * User: marco
 * Date: 25.04.13
 * Time: 12:57
 * To change this template use File | Settings | File Templates.
 */

namespace Hn\AssetBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * This type is for the internal uploading process.
 * Don't use it in any form.
 *
 * @package Hn\AssetBundle\Form
 */
class AssetEntityType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {
        $builder
            ->add( "files", "file" )

            ->add( "min-width", "hidden" )
            ->add( "max-width", "hidden" )
            ->add( "min-height", "hidden" )
            ->add( "max-height", "hidden" )

            ->add( "thumb-width", "hidden", array( "data" => 100 ) )
            ->add( "thumb-height", "hidden", array( "data" => 100 ) )

            ->add( "accept-type", "hidden", array( "data" => ".*/.*" ) )
            ->add( "maximum-size", "hidden", array( "data" => 10000000 ) );
    }

    public function finishView ( FormView $view, FormInterface $form, array $options ) {
        $view->children["files"]->vars["name"] .= "[]"; // make multiple possible (hack)
    }

    public function getName () {
        return "asset";
    }

    public function setDefaultOptions ( OptionsResolverInterface $resolver ) {
        $resolver->setDefaults( array( "csrf_protection" => false ) );
    }
}