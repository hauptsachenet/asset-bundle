<?php

namespace Hn\AssetBundle\Controller;


use Hn\AssetBundle\Form\AssetEntityType;
use Hn\AssetBundle\Form\AssetUploadType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hn\AssetBundle\Entity\Asset;
use Hn\AssetBundle\Form\AssetType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Asset controller.
 *
 * @Route("/")
 */
class AssetController extends Controller {

    /**
     * Stores the given file as an asset.
     *
     * @param Request $request
     * @return Response with informations about the file
     * @Route("/upload/", name="asset_upload")
     * @Method("POST")
     */
    public function uploadAction ( Request $request ) {
        /** @var $assetRepository \Hn\AssetBundle\Entity\AssetRepository */
        $assetRepository = $this->getDoctrine()->getRepository( 'Hn\AssetBundle\Entity\Asset' );
        $result = array(
            "files"  => array(),
            "errors" => array(),
            "warnings" => array()
        );

        // now accept the request
        $form = $this->createForm( new AssetEntityType() );
        $form->bind( $request );
        $assets = array();
        if ( $form->isValid() ) {
            $data = $form->getData();

            /** @var $file UploadedFile */
            foreach ( $data["files"] as $file ) {
                $checked = $this->checkFile( $form, $file );
                if ( $checked['error'] !== NULL ) {
                    $result["errors"][] = $checked['error'];
                } else {
                    if ( $checked['warning'] !== NULL ) {
                        $result["warnings"][] = $checked['warning'];
                    }
                    $assets[] = $assetRepository->createAsset( $file );
                }
            }
            // and save the assets
            $this->getDoctrine()->getManager()->flush();
            // now put them in the response
            /** @var $asset Asset */
            foreach ( $assets as $asset ) {
                $thumbnail = $this->get('liip_imagine.cache.manager')->getBrowserPath($asset->getPath(), 'thumbnail');
                $result["files"][] = array(
                    "id"    => $asset->getId(),
                    "thumb" => $thumbnail,
                    "path"  => $asset->getWebPath(),
                    "mime"  => $asset->getMimeType()
                );
            }
        }
        // give errors on failure ( array + array ftw! )
        else $result["errors"] += $form->getErrors();

        return new Response( json_encode( $result ), 200, array( "Content-type" => "text/html" ) );
    }

    /**
     * Performs a check if the file is valid
     *
     * @param Form         $form
     * @param UploadedFile $file
     * @return string
     */
    private function checkFile ( Form $form, UploadedFile $file ) {

        $result =  array('warning' => null, 'error' => null);

        $logger = $this->container->get('hn_asset.asset_logger');


        $data = $form->getData();
        $filename = $file->getClientOriginalName();
        $mimeType = $file->getClientMimeType();
        $type = $data["accept-type"];
        $maxSize = intval( $data["maximum-size"] );

        // check the size
        if ( $file->getSize() > $maxSize ) {
            $result['error'] = "$filename is too big, max " . ceil( $maxSize / 1024 ) . " kB";
            return $result;
        }

        // check the image size if needed
        if (
            ( $data["min-width"] || $data["max-width"] || $data["min-height"] || $data["max-height"] )
            && preg_match( AssetType::typeToRegExp( AssetType::IMAGE ), $mimeType )
        ) {
            list( $width, $height ) = getimagesize( $file->getPathname() );

            $factor = min( $data["max-width"] / $width, 1.0 );
            $factor = min( $data["max-height"] / $height, $factor );
            $factor = max( $data["min-width"] / $width, $factor );
            $factor = max( $data["min-height"] / $height, $factor );
            $factor = min( 1.0, $factor );
            $finalWidth = $width * $factor;
            $finalHeight = $height * $factor;

            // if the size is now off it is invalid
            if (
                $finalHeight > $data["max-height"] || $finalHeight < $data["min-height"]
                || $finalWidth > $data["max-width"] || $finalWidth < $data["min-width"]
            ) {
                $result['warning'] = "The image ($width x $height) does not meet the requirements:\n"
                    . " minimum size: " . $data["min-width"] . " x " . $data["min-height"]
                    . " maximum size: " . $data["max-width"] . " x " . $data["max-height"]
                    . "\n\n"
                    . "Upload the image anyway?"
                    . "\n";
                return $result;
            }
            // else it is valid
            return $result;
        }

        // check the type
        $logger->info(sprintf('asset->type: %s', $type));
        $logger->info(sprintf('asset->type-addcslashes: %s', AssetType::typeToRegExp( $type )));
        $logger->info(sprintf('asset->mimeType: %s', $mimeType));
        if ( $type && !@preg_match( AssetType::typeToRegExp( $type ), $mimeType ) ) {
            $result['error'] = "$filename has the wrong type";
            return $result;
        }
    }
}
