<?php
/**
 * Created by JetBrains PhpStorm.
 * User: marco
 * Date: 09.04.13
 * Time: 09:29
 * To change this template use File | Settings | File Templates.
 */

namespace Hn\AssetBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AssetRepository extends EntityRepository {

    /**
     * Persists that asset or gets an already existing asset if it is the same.
     *
     * @param Asset $asset The asset that should be persisted.
     * @return Asset Either the specified one or an already existing one.
     */
    public function getAndAddAsset ( Asset $asset ) {
        // check if this file already exists
        $existingAsset = $this->findOneBy( array( "path" => $asset->getPath() ) );
        if ( $existingAsset != NULL ) {
            return $existingAsset;
        }

        // else persist that asset, it is new than
        $this->getEntityManager()->persist( $asset );
        return $asset;
    }

    public function createAsset ( UploadedFile $file ) {
        $asset = new Asset();
        $asset->setFile( $file );
        return $this->getAndAddAsset( $asset );
    }
}