<?php

namespace Hn\AssetBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An Asset represents a file.
 * It should not (and can not) be changed. Instead create new instances.
 * If the asset gets removed it will remove the file with it.
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Asset {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * @Assert\File(maxSize="1000000")
     */
    protected $file;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=false, unique=true)
     */
    private $md5;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $references = 0;

    /**
     * Gets the file of this asset.
     *
     * @return mixed
     */
    public function getFile () {
        return $this->file;
    }

    /**
     * Sets the file for this asset.
     *
     * @param $file
     * @return $this
     */
    public function setFile ( $file ) {
        if ($this->file !== NULL) {
            throw new \Exception( "An asset can't be reassigned! Use a new one instead!" );
        }
        $this->file = $file;
        return $this;
    }


    public function getAbsolutePath () {
        return NULL === $this->path ? NULL
            : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath () {
        return NULL === $this->path ? NULL
            : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadRootDir () {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir () {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/assets';
    }

    /**
     * If a file is now in this asset a file name will be generated here.
     *
     * @ORM\PrePersist()
     * //@ORM\PreUpdate() // updates arent allowed
     */
    public function preUpload () {
        if ( NULL !== $this->file ) {
            // do whatever you want to generate a unique name
            $filename = sha1( uniqid( mt_rand(), true ) );
            $this->setPath( $filename . '.' . $this->file->guessExtension() );
        }
    }

    /**
     * After the asset was saved the file will be moved to it's final location by this method.
     *
     * @ORM\PostPersist()
     * //@ORM\PostUpdate() // updates aren't allowed
     */
    public function upload () {
        if ( NULL === $this->file ) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move( $this->getUploadRootDir(), $this->path );

        unset( $this->file );
    }

    /**
     * After remove of this asset the file will be removed
     *
     * @ORM\PostRemove()
     */
    public function removeUpload () {
        if ( $file = $this->getAbsolutePath() ) {
            unlink( $file );
        }
    }

    /**
     * Set the createdAt time. Should not be used from outside.
     *
     * @param \DateTime $createdAt
     * @return Asset
     */
    private function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Sets the path of the image. As it will be auto generated this method is private.
     *
     * @param string $path
     * @return Asset
     */
    private function setPath ( $path ) {
        $this->path = $path;
        return $this;
    }


    /*
     * generated code from here
     */

    /**
     * Get id
     *
     * @return integer
     */
    public function getId () {
        return $this->id;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath () {
        return $this->path;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

}