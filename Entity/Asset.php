<?php

namespace Hn\AssetBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Hn\AssetBundle\Form\AssetType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An Asset represents a file.
 * It should not (and can not) be changed. Instead create new instances.
 * If the asset gets removed it will remove the file with it.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Hn\AssetBundle\Entity\AssetRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Asset
{

    public static $uploadDir = 'uploads/assets';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var File
     *
     * @Assert\File(maxSize="5000000")
     */
    protected $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false, unique=true)
     */
    private $path;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $referenceCount = 0;

    /**
     * Gets the file of this asset.
     *
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Gets the file name without extension and path
     *
     * @return string
     */
    public function getFileName()
    {
        return preg_replace("/\\.\\w+$/m", "", $this->getPath());
    }

    public function  getFileExtension()
    {
        return pathinfo($this->getPath(), PATHINFO_EXTENSION);
    }

    /**
     * Gets the absolute path to the image.
     *
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return NULL === $this->path ? NULL
            : self::getUploadRootDir() . '/' . $this->path;
    }

    /**
     * Gets the public path to the image
     *
     * @return null|string
     */
    public function getWebPath()
    {
        return NULL === $this->path ? NULL
            : '/' . $this->getUploadDir() . '/' . $this->path;
    }

    public static function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../../../web/' . self::getUploadDir();
    }

    protected static function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return self::$uploadDir;
    }

    /**
     * After the asset was saved the file will be moved to it's final location by this method.
     *
     * @ORM\PostPersist()
     * //@ORM\PostUpdate() // updates aren't allowed
     */
    public function upload()
    {
        if (!$this->file instanceof UploadedFile) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move(self::getUploadRootDir(), $this->path);

        unset($this->file);
    }

    /**
     * After remove of this asset the file will be removed
     *
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Sets the file for this asset.
     *
     * @param File $file
     * @return $this
     * @throws \Exception if a file was already assigned
     */

    public function setFile(File $file)
    {
        if ($this->file !== NULL) {
            throw new \Exception("An asset can't be reassigned! Use a new one instead!");
        }
        $this->file = $file;
        // create a file name out of the file content
        $filename = sha1_file($file->getPathname());
        $this->setPath($filename . '.' . $this->file->guessExtension());
        return $this;
    }

    /**
     * Set the createdAt time. Should not be used from outside.
     *
     * @param \DateTime $createdAt
     * @return Asset
     */
    private function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Sets the path of the image. As it will be auto generated this method is private.
     *
     * @param string $path
     * @return Asset
     */
    private function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Set referenceCount
     *
     * @deprecated only there so doctrine doesn't create it
     * @param integer $referenceCount
     * @return Asset
     */
    private function setReferenceCount($referenceCount)
    {
        $this->referenceCount = $referenceCount;

        return $this;
    }

    /**
     * increases the reference counter used to see if this entity is needed.
     */
    public function addReference()
    {
        $this->referenceCount++;
    }

    /**
     * decreases the reference counter which can result into removal of this entity.
     * TODO implement removal with cron job
     *
     * @throws \Exception if the counter has gone under 0 as this is a programming error than.
     */
    public function removeReference()
    {
        $this->referenceCount--;
        if ($this->referenceCount < 0) {
            throw new \Exception("unneeded reference decrease, may be programming error");
        }
    }

    public function getMimeType()
    {
        $finfo = new \finfo(FILEINFO_MIME);
        if (file_exists($this->getAbsolutePath())) {
            $type = $finfo->file($this->getAbsolutePath());
            $type = preg_replace("/;.*$/m", "", $type);
            return $type;
        } else {
            return null;
        }
    }



    /*
     * generated code from here
     */

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Gets the number of references to this asset.
     *
     * @return integer
     */
    public function getReferenceCount()
    {
        return $this->referenceCount;
    }
}